### Description

A Front-End blogging website that allows users to post blog contents from anywhere with internet access. Locations are auto determined using GPS longitude and latitude, giving precise locations to automate the process without needing to enter in closest location or place that the user is currently at.

| Sample Picture                        | Sample Picture                     |
| ------------------------------------- | ---------------------------------- |
| ![Alt Text](./images/landingpage.png) | ![Alt Text](./images/homepage.png) |
| ![Alt Text](./images/main.png)        | ![Alt Text](./images/blog.png)     |

## Purpose and Use Case

Originally, this project was a group project in which me and Jarren did for **Web and Database Computing** where we we were taught to implement a blogging websites using Google API. I was fascinated by the fact of how Instagram's implementation of the location was done at the time and wanted to inspire new bloggers to use this software and show lists of locations similar to Instagram.

While this integrates the use of Google API into a blogging website, there are many variations to integrating this to similar projects:

- Use of paginations to show blogs per page
- Use of Google API to show location of the place when users are looking for places (Flatmates, real estates .etc)
- Integrating into job applications where users can predict/calculate travel distance from his/her current location to the company's location

This project is designated for users to use without needing to install any other third parties software or even running a backend database as this uses JSON file (`Places_Visited`). This way, the structure that will be used for grabbing information from Back-End RESTful API will be the same.

**As of 2019, the API key that I had used is no longer supported and therefore, locations will not work unless it's updated.**
**As we completed this course, we had the opportunity to showcase our portfolio on OPEN DAY 2017.**

## Project setup

When cloning this project, a clean installation of the project is done by running the following command:

```
npm install
```

## Running project

By default, this project is setup with NodeJs at the time of making, therefore the users have 2 options to start the applications by running:

```
./bin/www
```

This will then allow the user to visit the project site at

```
localhost:3000
```
